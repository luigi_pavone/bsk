package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>(10);
		this.firstBonusThrow = 0;
		this.secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {

		int sum = 0;
		
		if(frames.get(9).isSpare()) {
			frames.get(9).setBonus(this.firstBonusThrow);
		}
		
		if(frames.get(9).isStrike()) {
			frames.get(9).setBonus(this.firstBonusThrow + this.secondBonusThrow);
		}
		
		for(int i=8; i>=0; i--) {			
			if(frames.get(i).isSpare()) {
				frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
			} 
			else if(frames.get(i).isStrike() && frames.get(i+1).isStrike()) {

				if(i+1 == 9) {
					frames.get(i).setBonus(10 + this.firstBonusThrow);
				} else {
					frames.get(i).setBonus( frames.get(i+2).getFirstThrow() + 10);	
				}
					
			} else if(frames.get(i).isStrike()) {		
				frames.get(i).setBonus(frames.get(i+1).getFirstThrow() + frames.get(i+1).getSecondThrow());
			}
		}
		for(int i=0; i<10; i++) {
			sum += frames.get(i).getScore();
		}
		
		return sum;
	}

}
