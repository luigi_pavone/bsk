package bsk;

import static org.junit.Assert.*;
import org.junit.Test;
import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;

public class FrameTest {
	
	@Test
	public void firstThrowShouldBe2() throws BowlingException{
		Frame frame = new Frame(2,4);
		assertEquals(2, frame.getFirstThrow());
	}
	
	@Test
	public void secondThrowShouldBe4() throws BowlingException{
		Frame frame = new Frame(2,4);
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void scoreShouldBe8() throws BowlingException{
		Frame frame = new Frame(2,6);
		assertEquals(8, frame.getScore());
	}
	
	@Test
	public void frameShouldBeSpare() throws BowlingException{
		Frame frame = new Frame(8,2);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void frameShouldBeStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}

}
