package bsk;

import static org.junit.Assert.*;
import org.junit.Test;
import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testGameCostructionFirstThrowSecondFrameShouldBe7() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(7,2)); //index 1
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(9,1));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(7,3));
		game.addFrame(new Frame(5,2));
		
		assertEquals(7, game.getFrameAt(1).getFirstThrow());
	}
	
	@Test
	public void testGameCostructionSecondThrowLastFrameShouldBe2() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(5,3)); 
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(9,1));
		game.addFrame(new Frame(6,2));
		game.addFrame(new Frame(7,3));
		game.addFrame(new Frame(5,2)); //index 9
		
		assertEquals(2, game.getFrameAt(9).getSecondThrow());
	}
	
	@Test
	public void gameScoreShouldBe81() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void frameBonusShouldBe3() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.calculateScore();
		
		assertEquals(3, game.getFrameAt(0).getBonus());
	}
	
	@Test
	public void spareFrameScoreShouldBe13() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		game.calculateScore();
		
		assertEquals(13, game.getFrameAt(0).getScore());
	}
	
	@Test
	public void gameScoreShouldBe88WithFrameSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe104WithMultipleSpares() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));//spare
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(6,4));//spare
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(0,10));//spare
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(104, game.calculateScore());
	}
	
	@Test
	public void strikeFrameScoreShouldBe19() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		game.calculateScore();
		
		assertEquals(19, game.getFrameAt(0).getScore());
	}
	
	@Test
	public void gameScoreShouldBe94WithStrikeFrame() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe114WithMultipleStrikes() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(114, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe118WithStrikeAndSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6)); 
		
		assertEquals(118, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe103WithStrikeFollowedBySpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe112WithConsecutiveStrikes() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe131WithConsecutiveStrikes() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(4);
		game.setSecondBonusThrow(2);
		
		assertEquals(135, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe98WithConsecutiveSpares() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void firstBonusThrowShouldBe7() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8)); 
		game.setFirstBonusThrow(7);
		
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void secondBonusThrowShouldBe2() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0)); 
		game.setSecondBonusThrow(5);
		game.setSecondBonusThrow(2);
		
		assertEquals(2, game.getSecondBonusThrow());
	}
	
	@Test
	public void gameScoreShouldBe90WithLastFrameSpareBonusThrow7() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8)); 
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe92WithLastFrameStrikeFirstBonusThrow7SecondBonusThrow2() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4)); 
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0)); 
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBeAPerfectScore300() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0)); 
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0)); 
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe0() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0)); 
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0));
		game.addFrame(new Frame(0,0)); 
		
		assertEquals(0, game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe134() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(3,5));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,3)); 
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(9,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,3));
		game.addFrame(new Frame(5,5)); 
		game.setFirstBonusThrow(6);
		
		assertEquals(134, game.calculateScore());
	}

}
